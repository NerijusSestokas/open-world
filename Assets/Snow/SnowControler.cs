﻿using UnityEngine;
using System.Collections;

public class SnowControler : MonoBehaviour {
    public ParticleSystem par;

    void Start()
    {
        par.enableEmission = false; // Siegas išjungtas pradžioje
    }

    // Update is called once per frame
    void Update()
    {
        var rotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);  // Užrakinama, kad nesisuktu snaigės kartu su žaidėju
        par.transform.rotation = rotation;
        if (par.enableEmission == true && par.emissionRate < 1000)      // Didinamas snaigių kritimas
            par.emissionRate++;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "SnowTriger")    // Tikrinama ar žaidėjas perėjo trigerį
        {
            if (par.enableEmission == false)    // Kai žaidėjas kerta trigerį ir nesninga, pradeda snigt
            {
                par.enableEmission = true;
                GameObject.Find("Bridge").SendMessage("Turn", true);
            }
            else                    // Kitu atveju išjungiamas sniegas
            {
                par.emissionRate = 100;
                par.enableEmission = false;
                GameObject.Find("Bridge").SendMessage("Turn", false);
            }
        }
    }
}
