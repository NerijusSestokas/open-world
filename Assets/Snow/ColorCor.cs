﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.ImageEffects
{ 
    public class ColorCor : MonoBehaviour
    {
        public AnimationCurve red = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));     // Dieną raudonos spalvos korekcijos kreivė
        public AnimationCurve blue = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));    // Dieną mėlynos spalvos korekcijos kreivė
        public AnimationCurve green = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));   // Dieną žalios spalovs korekijos kreivė

        public AnimationCurve red2 = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));    // Naktį raudonos spalvos korekcijos kreivė
        public AnimationCurve blue2 = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));   // Naktį mėlynos spalvos korekcijos kreivė
        public AnimationCurve green2 = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));  // Naktį žalios spalovs korekijos kreivė
        bool nigh = false;
        // Use this for initialization
        void Start() {      }
        // Update is called once per frame
        void Update()
        {
            ColorCorrectionCurves cam = this.GetComponent<ColorCorrectionCurves>();
            if (!nigh)
            {
                cam.redChannel = red;           // Priskiriami kintamieji "ColorCorection" scriptui kai ne naktis
                cam.blueChannel = blue;
                cam.greenChannel = green;
                cam.UpdateParameters();         // Priskyrimai atnaujinami
            }
            else
            {
                cam.redChannel = red2;      // Priskiriami kintamieji "ColorCorection" scriptui kai naktis
                cam.blueChannel = blue2;
                cam.greenChannel = green2;
                cam.UpdateParameters();     // Priskyrimai atnaujinami
            }
        }
        // Kreipiasi DayNightCycle, kai pereinama į naktį, dieną
        void Night(bool nig) 
        {
            nigh = nig;
        }
    }
}