﻿using UnityEngine;
using System.Collections;

public class TurnOnOfSnow : MonoBehaviour {
	void Start () {
        gameObject.GetComponent<ParticleSystem>().enableEmission = false;
	}
	
	// Update is called once per frame
	void Update () {
        var rotation = Quaternion.LookRotation(Vector3.down, Vector3.forward);
        transform.rotation = rotation;
	}
}
