﻿using UnityEngine;
using System.Collections;

public class MoonMovment : MonoBehaviour {

    public float distance = 1000.0f;        // Kokiu atsumu mėnulis turi būt
    public float size = 100.0f;             // Kokio jis dydžio turi būt
    public float intensivity = 0;           // Jo skelidžiamos šviesos intensyvumas
    bool night = false;                     // Ar naktis
    float minutes;                          // Paros ciklo ilgumas mintuėmis
	// Use this for initialization
	void Start () {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, distance);
        transform.localScale = new Vector3(size, size, size);
	}
    // Keičiama mėnulio šiesos intensyvumas
    void Update() 
    {
        Light l = GetComponent<Light>();
        float tmp = intensivity / (minutes * 60f);
        if (night)
        {
            if (l.intensity < intensivity)
                l.intensity = l.intensity + tmp;
            else
                l.intensity = intensivity;
        }
        else
        {
            if (l.intensity > intensivity)
                l.intensity = l.intensity - tmp;
            else
                l.intensity = 0;

        }
    }
    // Pasikeičia, kai pereina iš nakties į dieną ir atvirkščiai
    void Night(bool nigh)
    {
        night = nigh;
    }
    // Sužino paros ciklo ilgumą
    void MinutesInCycle(float min)
    {
        minutes = min;
    }
}
