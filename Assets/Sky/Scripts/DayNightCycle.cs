﻿using UnityEngine;
using System.Collections;

public class DayNightCycle : MonoBehaviour
{


    public float minutesInCycle = 1.0f;         // Kokio ilgio mintėmis paros ilgis
    public float LightDayIntensivity = 1.0f;    // Dienos šviesos intensyvumas
    float timer;                // Laikmatis
    float precentageOfDay;      // Kiek praėjo ciklo
    float turnSpeed;            // Sukimosi greitis                
    float Intensivity = 0.0f;   // Naktį, saulės šviesumo intensyvumas
	// Use this for initialization
	void Start () 
    {
        timer = 0.0f;
        GameObject.Find("Moon").SendMessage("MinutesInCycle", minutesInCycle);  // Nusiučnia mėnuliu, kokio ilgio ciklas
	}
	
	// Update is called once per frame
	void Update () 
    {
        CheckTime();
        UpdateLights();
        turnSpeed = 360.0f / (minutesInCycle * 60.0f) * Time.deltaTime;
        transform.RotateAround(transform.position, transform.right, turnSpeed);
	}
    // Patikrina ar naktis
    bool isNight()
    {
        if (precentageOfDay > 0.5f)
        {
            GameObject.Find("FirstPersonCharacter").SendMessage("Night", true);     // Persiunčia žaidėjo kamerai, kad naktis
            GameObject.Find("Moon").SendMessage("Night", true);         // Persiunčia, mėnuliu, kad turi patekėt
            return true;
        }
        GameObject.Find("Moon").SendMessage("Night", false);        // Persiunčia, kad mėnulis turi nusileist
        GameObject.Find("FirstPersonCharacter").SendMessage("Night", false);    // Persiunčia žaidėjui, kad diena
        return false;
    }
    // Atnaujina šviesos intesyvumus
    void UpdateLights()
    {
        Light l = GetComponent<Light>();
        float IntesivityPlus = LightDayIntensivity / (minutesInCycle * 60.0f);
        if(isNight())
        {
            if (isNight())
            {
                Intensivity -= IntesivityPlus;
                if (Intensivity < 0)
                    Intensivity = 0;
            }
        }
        else if (!isNight())
        {
            Intensivity += IntesivityPlus;
            if (Intensivity > LightDayIntensivity)
                Intensivity = LightDayIntensivity;
        }
        l.intensity = Intensivity;
    }
    // Patikrinamas paros laikas
    void CheckTime() 
    {
        timer += Time.deltaTime;
        precentageOfDay = timer / (minutesInCycle * 60.0f);
        if (timer > (minutesInCycle * 60.0f))
            timer = 0.0f;
    }
}
