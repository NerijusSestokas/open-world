﻿using UnityEngine;
using System.Collections;

public class keitimas : MonoBehaviour {
	// Use this for initialization
    bool snow = false;  // Ar sninga
    float tmp = 1;      // Snigimo skaičius
    float tmp2 = 0;     // Žolės auginimo skaičius
    Renderer rend;
	void Start () {
        rend = GetComponent<Renderer>();
        rend.enabled = true; 
	}
	
	// Update is called once per frame
	void Update () {
        if (!snow)
        {
            if (tmp2 < 1)   // Kai nesninga auginama žolė
            {
                rend.material.SetFloat("Keit2", tmp2);
                tmp2 += 0.005f;
            }
            if (tmp < 1)    // Kai nesninga tiltas gauna medžio tekstūrą
            {
                tmp += 0.001f;
                rend.material.SetFloat("Keit", tmp);
            }
        }
        else
        {
            if (tmp2 > 0)   // Kai sninga pasitraukia žolė
            {
                rend.material.SetFloat("Keit2", tmp2);
                tmp2 -= 0.005f;
            }
            if(tmp > 0)     // Kai sninga tiltas pasidengia sniegu
                tmp -= 0.001f;
            rend.material.SetFloat("Keit", tmp);
        }
	}
    // Ar pradėjo snigti
    void Turn(bool tur)
    {
        snow = tur;
    }
}
