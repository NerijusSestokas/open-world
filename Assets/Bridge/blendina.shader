﻿Shader "blendina" {
   Properties {
      _MainTex ("RGBA Texture Image", 2D) = "white" {}	// Tilto tekstūra
	  _MyTex ("Texture", 2D) = "white" {}		// Sniego tekstūra
	  _MyGrass ("Texture", 2D) = "white" {}		// Žolės tekstūra
	  Keit ("Unique Identifier", float) = 1.0	// Apsnigimo skaičius
	  Keit2 ("Grass Identifier", float) = 0.0	// Žolės auginimo skaičius
   }
   SubShader {
      Pass {
         CGPROGRAM
         #pragma vertex vert  
         #pragma fragment frag
         uniform sampler2D _MainTex;    
		 uniform sampler2D _MyTex;
		 uniform sampler2D _MyGrass;
		 uniform float Keit;
		 uniform float Keit2;
         struct vertexInput {
            float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
         };
         vertexOutput vert(vertexInput input)
         {
            vertexOutput output;
            output.tex = input.texcoord;
            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
         }
         float4 frag(vertexOutput input) : COLOR
         {
            float4 textureColor1 = tex2D(_MainTex, input.tex.xy);  
			float4 textureColor2 = tex2D(_MyTex, input.tex.xy);
			float4 textureColor3 = tex2D(_MyGrass, input.tex.xy);
			return lerp(lerp(textureColor2, textureColor1, Keit), textureColor3, Keit2);	// Trijų tekstūrų sulieimas pagal kintamuosius
		 }
         ENDCG
      }
   }
   Fallback "Unlit/Texture"
}