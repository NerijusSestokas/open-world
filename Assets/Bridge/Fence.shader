﻿Shader "Cg Unlit" {
   Properties {
      _MainTex ("RGBA Texture Image", 2D) = "white" {}
	  _MyTex ("Texture", 2D) = "white" {}
   }
   SubShader {
      Pass {
 
         CGPROGRAM
 
         #pragma vertex vert  
         #pragma fragment frag
 
         uniform sampler2D _MainTex;    
		 uniform sampler2D _MyTex;
 
         struct vertexInput {
            float4 vertex : POSITION;
            float4 texcoord : TEXCOORD0;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 tex : TEXCOORD0;
         };
 
         vertexOutput vert(vertexInput input)
         {
            vertexOutput output;
 
            output.tex = input.texcoord;
            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
         }
 
         float4 frag(vertexOutput input) : COLOR
         {

            float4 textureColor1 = tex2D(_MainTex, input.tex.xy);  
			float4 textureColor2 = tex2D(_MyTex, input.tex.xy);
            return (textureColor2 > 0.5) * (1 -(1-textureColor1) / (2*(textureColor2-0.5))) +(textureColor2 <= 0.5) * (textureColor1 / (1-2*textureColor2));
		  //  return textureColor1;
         }
 
         ENDCG
      }
   }
   Fallback "Unlit/Texture"
}