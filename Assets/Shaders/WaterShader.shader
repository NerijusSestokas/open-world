﻿Shader "Water" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _ReflectColor ("Reflection Color", Color) = (1,1,1,0.5)
    _MainTex ("Base (RGB) RefStrength (A)", 2D) = "white" {}
    _Cube ("Reflection Cubemap", Cube) = "_Skybox" { TexGen CubeReflect }
    _BumpMap ("Normalmap", 2D) = "bump" {}
	_WaveSize ("WaveSize", float) = 0.25
	_WaveSize2 ("WaveSize2", float) = 0.25
	_WaveSpeed ("WaveSpeed", float) = 5
	_WaveSpeed2 ("WaveSpeed2", float) = 5
	_K("K", float) = 1
	_K2("K2", float) = 2
}
 
SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
    Cull Off
    LOD 300
   
 
CGPROGRAM
#pragma surface surf Lambert alpha vertex:vert
 
sampler2D _MainTex;
sampler2D _BumpMap;
samplerCUBE _Cube;
float _WaveSize;
float _WaveSize2;
float _WaveSpeed;
float _WaveSpeed2;
float _K;
float _K2;
 
fixed4 _Color;
fixed4 _ReflectColor;
 
struct Input {
    float2 uv_MainTex;
    float2 uv_BumpMap;
    float3 worldRefl;
    INTERNAL_DATA
};

void vert (inout appdata_full v) {
    float phase = _Time * _WaveSpeed;
	float phase2 = _Time * _WaveSpeed2;
    float offset = (v.vertex.x * 0  + v.vertex.z);
	float offset2 = (v.vertex.x  + v.vertex.z * 0);
	float x = v.vertex.x;
	float z = v.vertex.z;
	v.vertex.y = pow(((sin(offset + phase)+1)/2) * _WaveSize, _K) + pow(((sin(offset2 + phase2)+1)/2) * _WaveSize2, _K2);
}
 
void surf (Input IN, inout SurfaceOutput o) {
    fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
    fixed4 c = tex * _Color;
    o.Albedo = c.rgb;
   
    o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
   
    float3 worldRefl = WorldReflectionVector (IN, o.Normal);
    fixed4 reflcol = texCUBE (_Cube, worldRefl);
    o.Emission = reflcol.rgb * _ReflectColor.rgb;
    o.Alpha = reflcol.a * 0.9;
}
ENDCG
}
 
FallBack "Reflective/VertexLit"
}