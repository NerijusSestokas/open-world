﻿using UnityEngine;
using System.Collections;

public class FogColor : MonoBehaviour {

    public ParticleSystem ps;       // Kuriai Particle system tai bus taikoma
    public Color StartColor;
    public Color MiddleColor;
    public Color EndColor;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        ParticleSystem.Particle[] sar = new ParticleSystem.Particle[ps.particleCount];
        ps.GetParticles(sar);
        for (int i = 0; i < sar.Length; ++i)
        {
            float life = (sar[i].lifetime / sar[i].startLifetime);  // Paskaiciuojama kiek gyvens dalele
            sar[i].color = Color.Lerp(Color.Lerp(Color.red, Color.yellow, life), Color.green, life);    // Jos spalva sujungiama su kitom per laika
        }
        ps.SetParticles(sar, ps.particleCount);
        
	}
}
