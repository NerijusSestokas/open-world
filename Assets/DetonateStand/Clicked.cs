﻿using UnityEngine;
using System.Collections;

public class Clicked : MonoBehaviour {

    public GameObject bare;         // Kuris objektas isnyks susprogus
    public ParticleSystem part;     // Sprogimo particle
    int tmp = 0;
	void Start () 
    {
        part.playbackSpeed = 0.8f;	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit ;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100))     // Tikrina ar paspaude mygtuka
            {
                if (hit.transform.gameObject.name == this.gameObject.name && tmp < 1)
                {
                    Destroy(bare, 0.2f);
                    part.Play();
                    tmp++;
                }

            }
        }
	}
}
