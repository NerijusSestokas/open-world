﻿using UnityEngine;
using System.Collections;

public class Spray : MonoBehaviour {

    public ParticleSystem part;     // Geizerio particle
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {   
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 10))     // Tikrina ar paspaude mygtuka
            {
                if(hit.collider.name == gameObject.name)
                    part.Play();
            }
        }
    }
}
