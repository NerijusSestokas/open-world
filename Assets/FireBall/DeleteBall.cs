﻿using UnityEngine;
using System.Collections;

public class DeleteBall : MonoBehaviour
{
    void OnEnable()
    {
        Invoke("Destroy", 5f);      // Po 5 sekundziu rutulys bus isjungtas
    }

    void Destroy()
    {
        gameObject.SetActive(false);
    }
    void OnDisable()
    {
        CancelInvoke();
    }
}