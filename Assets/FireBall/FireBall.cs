﻿using UnityEngine;
using System.Collections;

public class FireBall : MonoBehaviour 
{
    public float speed = 40;     // Ugnies kamuolio greitis
    private Vector3 pos;
    private int ind;
    void Start ()
    {
        ind = 0;
    }

    void Update()
    {
        if (ind == 0)
        {
            pos = Camera.main.transform.forward;
            ind++;
        }
        else if (ind == 5)
            Start();
        Rigidbody rb = this.GetComponent<Rigidbody>();
        rb.velocity = pos * speed;
    }
}
