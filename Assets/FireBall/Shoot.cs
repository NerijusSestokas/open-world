﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Shoot : MonoBehaviour 
{
    public GameObject FireBall;         // Obejktas kuris yra ugnies kamuolys
    public Material BallMaterial;
    public Material BallMaterial2;
    public int BallAmount = 20;     // Kiek gali but ugnies kamuoliu
    public int BallSpeed = 40;      // Ugnies kamuoliu greitis
    public Color col1;
    public Color col2;
    List<GameObject> FireBalls;


    private ParticleSystem par;
    private int f = 1;
    private Material[] la;

    void Start () 
    {
        FireBalls = new List<GameObject>();         // Isskiriama atmintis ugnies kamuoliams
        for (int i = 0; i < BallAmount; i++)
        {
            GameObject fi = (GameObject)Instantiate(FireBall);
            fi.SetActive(false);
            FireBalls.Add(fi);
        }
	}

    void Update()
    {
        bool e = Input.GetKeyDown(KeyCode.E);
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            f = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            f = 2;
        }
        if (e)
        {
            Fire(BallSpeed);
        }
    }

    void Fire(int sp)
    {
        int speed = sp;
        for (int i = 0; i < FireBalls.Count; i++)
        {
            if (!FireBalls[i].activeInHierarchy)
            {
                par = FireBalls[i].GetComponent<ParticleSystem>();
                if (f == 1)
                {
                    FireBalls[i].GetComponent<Renderer>().material = BallMaterial;
                    par.startColor = col1;
                }
                else
                {
                    FireBalls[i].GetComponent<Renderer>().material = BallMaterial2;
                    par.startColor = col2;
                    speed = speed / 2;
                }
                FireBalls[i].transform.position = transform.position + Camera.main.transform.forward * 2;     // Pradine kamuolio pozicija pagal zaideja
                FireBalls[i].SetActive(true);
                Rigidbody rb = FireBalls[i].GetComponent<Rigidbody>();
                rb.velocity = Camera.main.transform.forward * speed;       // Kamuolio greitis ir kryptis
                break;
            }
        }
    }
}
