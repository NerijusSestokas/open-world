﻿using UnityEngine;
using System.Collections;

public class MouseLock : MonoBehaviour {
	// Use this for initialization
	void Start () {
        Vector2 posi;
        posi.x = Screen.height;
        posi.y = Screen.width;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.SetCursor(null, posi, CursorMode.Auto);
	}
}
